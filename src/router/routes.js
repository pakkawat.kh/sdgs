const routes = [
  // {
  //   path: '/',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [
  //     { path: '', component: () => import('pages/Index.vue') }
  //   ]
  // },
  {
    path: "/",
    component: () => import("pages/login.vue"),
  },
  {
    path: "/examinee",
    component: () => import("pages/examinee.vue"),
    name: "examinee",
  },
  {
    path: "/examination",
    component: () => import("pages/examination.vue"),
    name: "examination",
  },
  {
    path: "/user",
    component: () => import("pages/user.vue"),
    name: "user",
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
